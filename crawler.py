# -*- coding: utf-8 -*-
import elasticsearch.helpers as es_helpers
import geoip2.database
import multiprocessing as mp
import os
import requests
import urllib3
from datetime import datetime, timezone
from elasticsearch import Elasticsearch, RequestsHttpConnection
from lxml import html
from requests_aws4auth import AWS4Auth


def load(task_queue: mp.JoinableQueue, job_id: str, input_: str = 'input.txt', signature: str = 'None') -> int:
    """A function to load URLs from a flat input file into a queue. Returns the number of lines added to the queue.

    :param task_queue: multiprocessing.JoinableQueue()
    :param job_id: str
    :param input_: str
    :param signature: str
    :return: int
    """
    with open(input_, 'rt') as file:
        url_list = file.readlines()
        url_count = 0
        for line in url_list:
            url_count += 1
            url = line.strip()
            task_queue.put(Scan(url=url, job_id=job_id, signature=signature))
            # every 499 URLs added, add a 'ProcessResults' poison pill to trigger the consumer to process results
            if url_count == 499:
                task_queue.put('ProcessResults')
                url_count = 0
        return url_list.__len__()


def write_results(result_queue: mp.Queue):
    """A function to write results from a queue of results into both ElasticSearch and a local flat CSV file. Returns a
    dictionary containing an ElasticSearch action/data pair including scan results.

    :param result_queue: multiprocessing.Queue()
    :return: dict
    """
    bad_chars = str.maketrans('\\"\'\n\r', '?????')
    while not result_queue.empty():
        result = result_queue.get()
        result_dict = result['_source']
        with open('results.csv', 'at', encoding='utf-8', errors='replace') as f:
            f.write('{},\'{}\',\'{}\',{},\'{}\',{},\'{}\',{},{},{},{}\n'.format(
                result_dict['url'],
                str(result_dict['title']).translate(bad_chars),
                str(result_dict['description']).translate(bad_chars),
                result_dict['hasSignature'],
                result_dict['signature'],
                result_dict['httpStatus'],
                result_dict['errorMsg'],
                result_dict['location'],
                result_dict['country_code'],
                result_dict['asn_number'],
                result_dict['asn_org']
            ))
        yield result


class Consumer(mp.Process):

    def __init__(self, task_queue, result_queue):
        mp.Process.__init__(self)
        self.task_queue = task_queue
        self.result_queue = result_queue

    def run(self):
        proc_name = self.name
        while True:
            next_task = self.task_queue.get()
            if next_task is None:
                # Poison pill means shutdown
                print('{}: Exiting'.format(proc_name))
                self.task_queue.task_done()
                break
            elif next_task == 'ProcessResults':
                print('{}: Processing results'.format(proc_name))
                # Process results 'poison pill' but don't break out of loop
                run_es_results = write_results(self.result_queue)
                run_awsauth = AWS4Auth(os.environ['AWS_ACCESS_KEY'], os.environ['AWS_SECRET_KEY'], 'us-east-2', 'es')
                run_es = Elasticsearch(
                    hosts=[
                        {
                            'host': 'search-crawler-scan-results-2fwon56nlk3puydhkzmqob5m4i.us-east-2.es.amazonaws.com',
                            'port': 443,
                        }
                    ],
                    http_auth=run_awsauth,
                    use_ssl=True,
                    verify_certs=True,
                    connection_class=RequestsHttpConnection,
                    timeout=30,
                )
                for run_es_success, run_es_info in es_helpers.parallel_bulk(run_es, run_es_results):
                    if not run_es_success:
                        print('Doc failed', run_es_info)
                self.task_queue.task_done()
            if next_task != 'ProcessResults':
                print('{}: {}'.format(proc_name, next_task))
                answer = next_task()
                self.task_queue.task_done()
                self.result_queue.put(answer)


class Scan:

    def __init__(self, url, job_id, signature: str = 'None'):
        self.url = url
        self.ip = str()
        self.asn_number = str()
        self.asn_org = str()
        self.job_id = job_id
        self.signature = signature
        self.signature_detect = False
        self.site_title = str()
        self.site_desc = str()
        self.country_code = str()
        self.location = str()
        self.timestamp = datetime.now(timezone.utc)
        self.request_content = bytes()
        self.request_text = str()
        self.request_status = int()
        self.request_err = False
        self.request_err_msg = str()

    def __call__(self):
        try:
            # Get the document from the URL we were given.
            self.scan_get()
        except requests.ConnectionError as e:
            self.request_err = True
            self.request_err_msg = e.__str__()
        else:
            # If our request resolved into an IP, do a geo lookup on that IP and populate ASN and location properties
            if self.ip != '':
                self.scan_geo_get()
            # If our request wasn't an error, parse the request content for the page title and description
            if not self.request_err:
                self.metadata_parse()
            # And also check if the document carried the signature we are looking for
            if self.signature != 'None':
                self.signature_parse()
        finally:
            # This is the actual ElasticSearch document we're sending in our bulk API requests
            elastic_doc = {
                'url': self.url,
                'ip': self.ip,
                'asn_number': self.asn_number,
                'asn_org': self.asn_org,
                'title': self.site_title,
                'description': self.site_desc,
                'hasSignature': self.signature_detect,
                'httpStatus': self.request_status,
                'errorMsg': self.request_err_msg,
                'signature': self.signature,
                'country_code': self.country_code,
                'location': self.location,
                'timestamp': self.timestamp,
                'job_id': self.job_id
            }
            # This is the ElasticSearch action we wrap each document with so the bulk API knows what to do
            elastic_action = {
                '_index': 'scan-job',
                '_type': 'result',
                '_source': elastic_doc
            }
        return elastic_action

    def scan_get(self):
        """This method is called on a Scan object in order to populate the object's request properties.

        :return:
        """
        # Don't show warnings thrown by urllib3 when making GET requests, we don't care.
        urllib3.disable_warnings()
        target = self.url
        self.timestamp = datetime.now(timezone.utc)
        if self.url.startswith('http://') is False and self.url.startswith('https://') is False:
            target = 'http://{0}'.format(self.url)
        try:
            request = requests.get(url=target, timeout=3, verify=False, stream=True)
        except Exception as e:
            self.request_err = True
            self.request_err_msg = e.__str__()
            return

        # We access this protected function in order to obtain the IP address of a URL without having to make an extra
        #   request specifically for DNS lookup. Our initial GET request is all we need to get the IP.
        try:
            ip = request.raw._connection.sock.getpeername()  # pylint: disable=protected-access
        except AttributeError:
            pass
        else:
            self.ip = ip[0]

        # Check the status of the request. If it was successful, update the Scan object's request properties.
        #   If it was a failure, update the Scan object's request error properties with the error info.
        try:
            request.raise_for_status()
        except requests.exceptions.HTTPError as e:
            self.request_err = True
            self.request_err_msg = e.__str__()
            self.request_status = request.status_code
            return
        if request.ok:
            self.request_content = request.content
            self.request_text = request.text
            self.request_status = request.status_code
            return

    def scan_geo_get(self):
        """This method is called on a Scan object in order to populate the object's IP, ASN and geolocation coordinate
        properties.

        :return:
        """
        with geoip2.database.Reader('data\GeoLite2-City.mmdb') as reader:
            geodb = reader.city(self.ip)
            self.location = '{},{}'.format(geodb.location.latitude, geodb.location.longitude)
            self.country_code = geodb.country.iso_code
        with geoip2.database.Reader('data\GeoLite2-ASN.mmdb') as reader:
            geodb = reader.asn(self.ip)
            self.asn_number = geodb.autonomous_system_number
            self.asn_org = geodb.autonomous_system_organization

    def metadata_parse(self):
        """This method is for populating a Scan object's ``site_title`` and ``site_desc`` properties using the data from
        its ``request_content`` property. This should only be called after scan_get() has been called.

        :return:
        """
        try:
            root = html.fromstring(self.request_content)
        except html.etree.ParserError:
            return

        try:
            self.site_title = '{}'.format(root.xpath('/html/head/title')[0].text)
        except Exception:
            pass

        try:
            self.site_desc = '{}'.format(root.xpath('/html/head/meta[@name="description"]/@content')[0])
        except Exception:
            pass

    def signature_parse(self):
        """This method is for detecting if a given signature was included in the document we objected from the input URL.
        This should only be called after scan_get() has been called, since it relies on the ``request_text`` property.

        :return:
        """
        if self.request_text.find(self.signature) != -1:
            self.signature_detect = True
