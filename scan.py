import argparse
import crawler
import multiprocessing as mp
import os
from uuid import uuid4


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--infile', help='set a custom domain input file location',
                        default='input.txt', metavar='FILE')
    parser.add_argument('-j', '--job', help='provide your own job ID to be used as the Elastic index name',
                        default='{}'.format(uuid4().hex))
    parser.add_argument('-s', '--sig', help='provide a signature to scan each target for', action='store',
                        default='None', metavar='TEXT')
    parser.add_argument('-d', '--debug', help='enable debugging output to the console', action='store_true', )
    args = parser.parse_args()

    # Establish communication queues
    tasks = mp.JoinableQueue()
    results = mp.Queue()

    # Start consumers
    num_consumers = mp.cpu_count() * 2
    print('Creating {} consumers'.format(num_consumers))
    consumers = [
        crawler.Consumer(tasks, results)
        for i in range(num_consumers)
    ]
    for w in consumers:
        w.start()

    # Enqueue jobs
    num_jobs = crawler.load(tasks, input_=args.infile, job_id='{}'.format(args.job), signature=args.sig)

    # Add a poison pill for each consumer
    for i in range(num_consumers):
        tasks.put(None)

    # Wait for all of the tasks to finish
    tasks.join()

    es_results = crawler.write_results(results)
    awsauth = crawler.AWS4Auth(os.environ['AWS_ACCESS_KEY'], os.environ['AWS_SECRET_KEY'], 'us-east-2', 'es')
    es = crawler.Elasticsearch(
        hosts=[
            {
                'host': 'search-crawler-scan-results-2fwon56nlk3puydhkzmqob5m4i.us-east-2.es.amazonaws.com',
                'port': 443,
            }
        ],
        http_auth=awsauth,
        use_ssl=True,
        verify_certs=True,
        connection_class=crawler.RequestsHttpConnection,
        timeout=30,
    )
    for es_success, es_info in crawler.es_helpers.parallel_bulk(es, es_results):
        if not es_success:
            print('Doc failed', es_info)
